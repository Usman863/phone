'use strict';

const express = require('express');
const router = express.Router();
const phoneRoute = require('../../phones/route/api');
const newDocumentRoute = require('../../document/route/api')

router.use('/phones', phoneRoute);
router.use('/document',newDocumentRoute)
module.exports = router;
