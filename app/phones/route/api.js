'use strict';

const express = require('express');
const router = express.Router();
const phoneService = require('../service/phoneService');
const response = require('../phones.json')
// get phones
router.get('/', (req, res, next) => {
    let phoneData = {response}
    res.send(phoneData);
});

// get phone by Id
router.get('/:id', (req, res, next) => {
    const phoneId = req.params.id;
    return phoneService.getAPhone(phoneId)
        .then(data => {
            return res.send(data);
        }, err => next(err));
});

// update phone
router.put('/:id', (req, res, next) => {
    const phoneId = req.params.id;
    return phoneService.updatePhone(phoneId, req.body, req.user)
        .then(phone => {
            return res.send(phone);
        }, err => next(err));
});
// create phone
router.post('/', (req, res, next) => {
    return phoneService.createPhone(req.body)
        .then(data => {
            return res.send(data);
        }, err => next(err));
});
// delete phone
router.delete('/:id', (req, res, next) => {
    const phoneId = req.params.id;
    return phoneService.deletePhone(phoneId)
        .then(data => {
            return res.send(data);
        }, err => next(err));
});

module.exports = router;
