'use strict';

const RS = require('../../base/response/responseService');
const PhoneStorage = require('./../storage/PhoneStorage');
const phoneStorage = new PhoneStorage();
const phonePopulation = require('./../storage/populate/phonePopulation')

const createPhone = async (param, user) => {
    try {
        return phoneStorage.store(param, user, phonePopulation.find);
    } catch (err) {
        return Promise.reject(RS.errorMessage(err.message));
    }
};

const updatePhone = async (phoneId, param, user) => {
    try {
        const updateData = { ...param };
        return phoneStorage.update(phoneId, updateData, user, phonePopulation.find);
    } catch (err) {
        return Promise.reject(RS.errorMessage(err.message));
    }
};

const getAPhone = async (phoneId) => {
    try {
        const result = await phoneStorage.findOne({ _id: phoneId });
        return Promise.resolve({ result });
    } catch (err) {
        return Promise.reject(RS.errorMessage(err.message));
    }
};

const deletePhone = async (phoneId) => {
    try {
        let deletedPhone = await phoneStorage.delete(phoneId);
        if (deletedPhone) {
            return Promise.resolve(RS.successMessage('Phone deleted successfully'));
        }
    } catch (err) {
        return Promise.reject(RS.errorMessage(err.message));
    }
};

module.exports = {
    updatePhone,
    deletePhone,
    getAPhone,
    createPhone
};
