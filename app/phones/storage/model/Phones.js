'use strict';

const mongoose = require('mongoose');
const phoneSchema = mongoose.Schema({
    name: {
        type: String,
        sparse: true
    },
    model: {
        type: String
    },
    color: {
        type: String
    },
    specification: {
        type: String
    },
    title: {
        type: String
    },
    ptaApproved: {
        type: Boolean
    }
}, { timestamps: true });

phoneSchema.index({ name: 'text' });
const Phone = mongoose.model('Phone', phoneSchema);

module.exports = Phone;
