'use strict';

const BaseStorage = require('../../base/storage/BaseStorage');
const PhoneModel = require('./model/Phones');

class PhoneStorage extends BaseStorage {
    constructor (props) {
        super({ ...props, Model: PhoneModel });
    }

    store (data, Phone, populate) {
        return super.store(data, Phone, populate);
    }

    update (docId, updateData, Phone, populate) {
        return super.update(docId, updateData, Phone, populate);
    }
}

module.exports = PhoneStorage;
