'use strict';

const express = require('express');
const router = express.Router();
const upload = require('../../config/multerService')
const bucketurl = require('../../config/bucketUrl')

router.post('/',upload.any('pictures'), (req, res, next) => {
    let myfile = { fileURL: `${bucketurl}/images/${req.files[0].filename}` }
    console.log(myfile)
    res.send(myfile)
});

module.exports = router;
