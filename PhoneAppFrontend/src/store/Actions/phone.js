import Axios from "axios";
export const GetAllPhones = Values => async dispatch => {
    try {
        const response = (await Axios.get('http://localhost:4000/api/phones')).data;
        dispatch({
            type: "GET_ALL_PHONES",
            payload: response.response
        })

    } catch (error) {
        console.log(error)
    }
}