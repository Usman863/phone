import { combineReducers } from "redux";
import Phones from "./Reducers/phones_reducer";
const appReducer = combineReducers({
  Phones,
});
export default (state, action) => {
  return appReducer(state, action);
};
