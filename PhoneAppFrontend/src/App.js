import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import DataTableTemplatingDemo from "./components/DataTableTemplatingDemo/DataTableTemplatingDemo"

const App = () => {
  return (
    <div>
      <BrowserRouter>
        <Switch >
          <Route path='/' exact > <DataTableTemplatingDemo /> </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
};

export default App;
