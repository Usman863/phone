import React, { useState, useEffect } from 'react';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import 'primeicons/primeicons.css';
import 'primereact/resources/themes/saga-blue/theme.css';
import 'primereact/resources/primereact.css';
import 'primeflex/primeflex.css';
import './index.css'
import { Dialog } from 'primereact/dialog';
import './DataTableDemo.css'
import * as Actions from '../../store/Actions';
import { useDispatch, useSelector } from "react-redux";
export default function DataTableTemplatingDemo(props) {
    const [loading, setLoading] = useState(false);
    const [products, setProducts] = useState([])
    const [selectedProduct, setSelectedProduct] = useState(null)
    const [selectedRow, setSelectedRow] = useState(null)
    const [displayBasic, setDisplayBasic] = useState(false)
    const dispatch = useDispatch();

    const getPhonesData = useSelector((state) => state.Phones.phones);

    useEffect(() => {
        setLoading(true);
        dispatch(Actions.GetAllPhones());
    }, []);

    useEffect(() => {
        setProducts(getPhonesData)
        setLoading(false);
    }, [getPhonesData]);


    const formatCurrency = (value) => {
        return value.toLocaleString('en-US', { style: 'currency', currency: 'USD' });
    }

    const imageBodyTemplate = (rowData) => {
        return <img src={`${rowData.picture}`} onError={(e) => e.target.src = 'https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png'} alt={rowData.picture} className="product-image" />;
    }

    const priceBodyTemplate = (rowData) => {
        return formatCurrency(rowData.price);
    }
    const RowSelection = (e) => {
        setSelectedRow(e.value)
        setDisplayBasic(true)
    }
    const onHide = () => {
        setDisplayBasic(false)
    }
    const header = (
        <div className="table-header">
            Mobile Phones
        </div>
    );
    const footer = `In total there are ${products ? products.length : 0} products.`;
    return (

        <div className="datatable-templating-demo">
            <div className="card">
                <DataTable loading={loading} lazy value={products} selectionMode="single" selection={selectedProduct} onSelectionChange={e => { RowSelection(e); setSelectedProduct(e.value) }} dataKey="id" header={header} footer={footer}>
                    <Column field="picture" header="Image" body={imageBodyTemplate} ></Column>
                    <Column field="name" header="Name" ></Column>
                    <Column field="price" header="Price" body={priceBodyTemplate}></Column>
                    <Column field="color" header="Color"></Column>
                </DataTable>
            </div>
            <div>
                <Dialog header='Mobile' draggable={false} visible={displayBasic} onHide={() => onHide()}>
                    <div>
                        <img src={`${selectedRow?.picture}`} onError={(e) => e.target.src = 'https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png'} alt={selectedRow?.picture} className="product-image" />
                        <p>ID: <span className="font-of-text">{selectedRow?.id}</span></p>
                        <p>Name: <span className="font-of-text">{selectedRow?.name}</span></p>
                        <p>Color: <span className="font-of-text">{selectedRow?.color}</span></p>
                        <p>Description: <span className="font-of-text">{selectedRow?.description}</span></p>
                        <p>Image Name: <span className="font-of-text">{selectedRow?.imageFileName}</span></p>
                        <p>Manufacturer: <span className="font-of-text">{selectedRow?.manufacturer}</span></p>
                        <p>Price: <span className="font-of-text">{selectedRow?.price}</span></p>
                        <p>Processor: <span className="font-of-text">{selectedRow?.processor}</span></p>
                        <p>Ram: <span className="font-of-text">{selectedRow?.ram}</span></p>
                        <p>Screen Size: <span className="font-of-text">{selectedRow?.screen}</span></p>
                    </div>
                </Dialog>
            </div>
        </div>
    );

}