import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { createMuiTheme, ThemeProvider } from "@material-ui/core";

import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import './App.css'
import reducers from "./store/reducers.js";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducers, composeEnhancers(applyMiddleware(thunk)));

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#808080",
      light: "#d9dadb",
      dark: "#103349"
    },
    secondary: {
      main: "#103349",
      light: "#103349",
      dark: "#103349",
      contrastText:"#fff"
    },
    error:{
      main:"#d9061f",
      dark:"#d9061f"
    },
    action:{
      selected:"#103349"
    }

  },
});

ReactDOM.render(
  <Provider store={store}>

    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>,
    </Provider>,

  document.querySelector("#root")
);
